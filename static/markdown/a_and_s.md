## A&S
_Friday night at 7PM, campsite at the far end of the battlefield._
Lay On! (Amtgard RPG) one shot campaign with Lord Aarach Corvid Create a character and adventure through a one shot campaign run by game designer and writer Aarach Corvid (Sam Butler). There will also be discussion about how to take your persona from field to the tabletop and back again each week. Game is limited to 8 players.

_Friday night at 7PM, main hall._
Paint and Sip with Sir Nexus Conversation, wine, and creating a work of art instructed by Sir Nexus Crow, The Feathered. **There is a $10 fee for this class.**

_Saturday morning 8AM (ish) various locations (meet at front of tavern)_
Nature Walk with Sir Nexus Join Sir Nexus Crow, The Feathered on a walk of the Ye Olde Common campsite. Nexus is a naturalist and expert bird watcher- these walks are always informative and a great way to kick off your Saturday!

_Saturday at 1:00PM, Pavilion by the battlefield._
Beginning Waltz with Page Avalon Cayrel Come learn some basic waltz steps and technique in preparation for the Goblin Ball!

_Saturday 2-5PM, front of main hall._
Introduction to Pierce Work with Sir Castings
Pierce Work is a fundamental skill of traditional jewelry making used to make decorative holes in sheet materials. This course will cover all of the basics you need to begin creating jewelry and decorative accents of your very own! There are enough materials for people to work six at a time, with observation and discussion also encouraged. **Sir Castings will be accepting donations to defray materials cost.**

_Sunday at 1:00PM, meet at tavern_
Homemade Clay Making with Peachez
Learn how to make clay and get nice and muddy like any goblin should!   This class is to teach people how to make clay from home. There are many methods to making clay from materials found in a modern house, however this class you will need to hit your back yard or the nearest stream. By gathering mud from the side of a stream, we will be using basic sifting and drying methods to make moldable, oven baked clay.  **Peachez will be accepting donations to defray materials cost.**

_Sunday 1:30-3:30PM, main hall_
Needle tatting Basics with Mistress Melisandra Moirai
Come learn the art of lace making through tatting. A basic overview of tatting stiches and pattern reading will be taught. We will also be creating a small motif that can be used to adorn garments, accessories, or even to create jewelry. Participants make keep their materials to keep practicing at home.  Class size limit 10.  
