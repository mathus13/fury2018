## Meals
_Friday 6pm (Traveler's Supper) Black Harts volunteer meal-_
Zoodles and regular pasta with chicken, mushrooms, and Italian sauce (not tomato based).

_Saturday 9am Breakfast_
Quiche with spinach, onions, and mushrooms, toast, fresh fruit, and home fries.

_Saturday 12PM Lunch_
Taco bar of tortillas, marinated onions, ground beef, lettuce, cheese, sour cream, salsa, black beans, corn, and Spanish rice. Vegetarian option of portobello mushroom taco “meat”.

_Saturday 6PM Dinner The Marshals volunteer meal (TBD)_

_Sunday 9am Breakfast_
French toast, fresh fruit, syrup, breakfast sausage

_Sunday 12PM Lunch_
baked potato bar- potato, cheese, sour cream, butter, bacon bits, jalapeños, chili, diced onions, and other toppings

_Sunday 6pm Feast_
Teriyaki chicken, vegetable lo mein, napa cabbage and toasted sesame salad.
Vegetarian option: Teryaki seitan and veggies

\*Tavern will have snacks and drinks for a fee, including the return of cold brew coffee by Mogis!
