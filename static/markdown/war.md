## War
Quest will be ongoing and interactive throughout the event! Roleplay! Talk to NPCs! Read signs!

Dungeon Crawls will be happening throughout the event into Saturday evening!

**Battlegame Schedule-**

Friday at 4 - Seize the Goblin Gates

Friday at 5 - Gates Skirmish

Saturday at 1 - Jareth’s Arena

Saturday at 3 - Park Battle

Saturday at 5 - Peasant Revolt

Saturday at 7 - gate defense.   Jareth loyalists are defenders

Sunday at 11 - Company Battle

Sunday at 1- Battle of Beasts

Sunday at 3 - Fight for the Throne
