export const state = () => ({
  events: [
    {
      key: '509686f2-0fec-4080-a6e7-03885fc79175',
      date: '2020-09-04',
      start: '2020-09-04 12:00',
      end: '2020-09-04 23:59',
      type: 'Troll',
      title: 'Troll is Open',
      details: 'Come register'
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc79176',
      date: '2020-09-04',
      start: '2020-09-04 13:00',
      end: '2020-09-04 23:59',
      type: 'War',
      title: 'Battlegame:	Open ditch, pick up games',
      details: 'Battlegame:	Open ditch, pick up games'
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc7917',
      date: '2020-09-05',
      start: '2020-09-05 09:00',
      end: '2020-09-05 09:59',
      type: 'Feast',
      title: 'Breakfast',
      details: ''
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc7918',
      date: '2020-09-05',
      start: '2020-09-05 10:00',
      end: '2020-09-05 10:59',
      type: 'War',
      title: 'NB Fighting Tourney Sign-ups Open',
      details: ''
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc7919',
      date: '2020-09-05',
      start: '2020-09-05 10:00',
      end: '2020-09-05 10:59',
      type: 'A&S',
      title: 'Iron Gryphon Sign-ups Open',
      details: ''
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc7920',
      date: '2020-09-05',
      start: '2020-09-05 10:00',
      end: '2020-09-05 10:59',
      type: 'A&S',
      title: 'Class',
      details: ''
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc7921',
      date: '2020-09-05',
      start: '2020-09-05 11:00',
      end: '2020-09-05 11:59',
      type: 'War',
      title: 'Valkyrie Fighting Tourney Sign-ups Close/Tourney Begins',
      details: ''
    },
    {
      key: '509686f2-0fec-4080-a6e7-03885fc7922',
      date: '2020-09-05',
      start: '2020-09-05 11:00',
      end: '2020-09-05 11:59',
      type: 'A&S',
      title: 'Iron Gryphon Sign-ups Close/Iron Gryphon Begins',
      details: ''
    }
  ]
})
export const mutations = {}
