export const state = () => ({
  crats: [
    {
      role: 'Autocrat',
      persona: 'Director Jace Lanes',
      page: '/crats/autocrat'
    },
    {
      role: 'Guru',
      persona: "Consular Jondal'ar Silvertongue, Defender of Goldenvale",
      page: '/crats/autocrat'
    },
    { role: 'A&S', persona: 'Weaver Wyrmwood', page: '/crats/a&s' },
    { role: 'Feast', persona: 'Nexus Crow', page: '/crats/feast' },
    {
      role: 'Medic',
      persona: 'Muirin Dariush Ayotte',
      page: '/crats/medic'
    },
    { role: 'Reeve', persona: 'Jake Meder', page: '/crats/reeve' },
    { role: 'Security', persona: 'Anders', page: '/crats/security' },
    {
      role: 'Troll',
      persona: 'Arglwydd Barwnes Incarsarus, Esq',
      page: '/crats/troll'
    },
    {
      role: 'Quest',
      persona: 'Xedram',
      page: '/crats/quest'
    }
  ]
})

export const mutations = {
  addCrat(state, crat) {
    if (!crat.position || !crat.persona) {
      return false
    }
    crat = Object.assign(
      {
        page: '/crats/' + crat.position.toLowerCase().replace(' ', '_'),
        img: null
      },
      crat
    )
    state.push(crat)
  },
  removeCrat(state, crat) {
    for (let i in state.crats) {
      if (
        state.crats[i].position == crat.position &&
        state.crats[i].persona == crat.persona
      ) {
        state.crats.splice(i, 1)
      }
    }
  }
}
